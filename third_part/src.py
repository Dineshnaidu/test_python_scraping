# Exercise 1
import requests

def http_request(url, params, headers):
    response = requests.post(url, data=params, headers=headers, verify=False)
    result = response.json()
    return result

data = {
    "msg":"welcomeuser",
    "isadmin":1
    }

# Uncomment below lines to run the file to check results of exercise 1

# headers = {'User-Agent': 'Mozilla/5.0'}
# print(http_request("https://httpbin.org/anything", data, headers))

# headers = {'User-Agent': 'Chrome/61.0.3163.100'}
# print(http_request("https://httpbin.org/anything", data, headers))

# Exercise 2
import json
import gzip
import csv
import pandas as pd

class ProductAnalyzer:
    def __init__(self):
        self.data_file = "data/data.json.gz"
        self.available_products = []
        self.unavailable_products = []

    def check_available_products(self):
        with gzip.open(self.data_file, "rb") as file:
            data = json.load(file)
            bundels = data["Bundles"]
            count = 0
            for rec in bundels:
                if rec.get("Products"):
                    products = rec.get("Products")
                else:
                    products = rec.get("Product")
                if products:
                    for product in products:
                        name = product.get("Name")
                        if name:
                            try:
                                instock = product["IsInStock"]
                                if instock:
                                    price = product.get("Price")
                                    price = round(price,1)
                                    product_detail = f"You can buy {name} at our store at {price}"
                                    self.available_products.append(product_detail)
                                else:
                                    self.unavailable_products.append({
                                      "product_id": product.get("Stockcode"),
                                      "product_name": name
                                    })
                                    id = product.get("Stockcode")
                                    print(f"Unavailable product id: {id} and name: {name}")
                            except Exception:
                                print(f"Error: Unavailable to get stock status for product {name}")
            
            filename = "avilable_products.csv"
            with open(filename, 'w', newline='') as csvfile: 
                csvwriter = csv.writer(csvfile) 
                csvwriter.writerow(["avilable_product"]) 
                for product in self.available_products:
                    csvwriter.writerow([product])

analyzer  = ProductAnalyzer()
# Uncomment below lines and run the file to check results of exercise 2

# analyzer.check_available_products()