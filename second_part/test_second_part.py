import pytest

from second_part.src import div, raise_something, add, ForceToList, random_gen, get_info, add, ProcessCheckMeta, ProcessCheckMeta

# Test case for Exersise 1
def test_generator():
    g = random_gen()
    assert isinstance(g, type((x for x in [])))
    a = next(g)
    while a != 15:
        assert 10 <= a <= 20
        a = next(g)
    with pytest.raises(StopIteration):
        next(g)

# Test case for Exersise 2
def test_to_str():
    assert add(5, 30) == '35'
    assert get_info({'info': [1, 2, 3]}) == '[1, 2, 3]'

# Test case for Exersise 3
def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)

# Test case for Exersise 4
def test_cache_decorator():
    cache_decorator = CacheDecorator()
    
    @cache_decorator
    def add(x,y):
        return x+y

    assert add(3,4) == 7
    assert add(3,4) == 7
    assert add(4,3) == 7

# Test case for Exersise 5
def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4

# test_case for Exercise 6
class SampleClass(metaclass = ProcessCheckMeta):
    def process(self, param1, param2):
        pass

def test_valid_class():
    obj = SampleClass()

def test_missing_process_method():
    with pytest.raises(AttributeError):
        class InvalidClass(metaclass = ProcessCheckMeta):
            pass

def test_invalid_process_method():
    with pytest.raises(TypeError):
        class InvalidClass(metaclass = ProcessCheckMeta):
            def process(self, param1, param2, param3):
                pass


