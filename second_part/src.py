# exercise 1
import random
def random_gen():
    while True:
        result = random.randint(10, 20)
        yield result
        if result == 15:
            return True

# exercise 2
def decorator_to_str(func):
    def inner_func(*args, **kwargs):
        res = func(*args, **kwargs)
        if not isinstance(res, str):
           res = str(res)
        return res
    return inner_func


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']

# exercise 3
def ignore_exception(exception):
    def decorator_func(func):
        def wrapper_func(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
            except exception as e:
                res = None
            return res
        return wrapper_func
    return decorator_func

@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b

@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

# exercise 5
class MetaInherList(type):
    def __new__(cls, name, bases, attrs):
        bases = bases + (list,)
        return super().__new__(cls, name, bases, attrs)


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

# exercise 6
import inspect
class ProcessCheckMeta(type):
    def __init__(cls, name, bases, attrs):
        if 'process' not in attrs:
            raise AttributeError(f"Class {name} must have a 'process' method")
        process = attrs["process"]
        if len(inspect.signature(process).parameters) != 3:
            raise TypeError(f"'process' method in class {name} must be 3")
        super().__init__(name, bases, attrs)      

