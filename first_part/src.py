# Exercise 1
def exercise_one():
    for i in range(1,101):
        if i%3 == 0:
            if i%5 == 0:
                print("ThreeFive")
            else:
                print("Three")
        elif i%5 == 0:
            print("Five")
        else:
            print(i)

# Exercise 2
def is_colorful(num):
    lst = list(map(int, str(num)))
    res = []
    mul = 1
    for i in range(len(lst)):
        if len(lst) > i + 1:
            res.append(lst[i] * lst[i + 1])
    for i in lst:
        res.append(i)
        mul = mul * i
    res.append(mul)
    print(res)
    print(set(res))
    if len(res) == len(set(res)):
        print("True")
    else:
        print("False")

# Exercise 3
def calculate(lst):
    sum = 0
    print(lst)
    if type(lst) is list:
        for i in range(len(lst)):
            if type(lst[i] is str):
                if lst[i].lstrip("-").isdigit():
                    sum = sum + eval(lst[i])
        return sum
    else:
        return False

# Exercise 4
def anagrams(val, lst):
    res= []
    for i in lst:
        if sorted(val) == sorted(i):
            res.append(i)
    return res
